import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import usbmux.UsbmuxNettyClient;

public class TestUsbmux {

    private static final Logger log = LoggerFactory.getLogger(TestUsbmux.class);

    @Test
    public void testListen() throws Exception {
        UsbmuxNettyClient client = new UsbmuxNettyClient("Listen Devices Client");

        client.listen((dev, ev) -> log.info("Device: {} {}", dev, ev));

        Thread.sleep(5000);
    }

    @Test
    public void testList() throws Exception {
        UsbmuxNettyClient client = new UsbmuxNettyClient("List Devices Client");

        log.info("{}", client.list());
    }
}
