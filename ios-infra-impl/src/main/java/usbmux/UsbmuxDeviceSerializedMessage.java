package usbmux;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UsbmuxDeviceSerializedMessage {
    public List<UsbmuxDeviceSerializedMessage> DeviceList;
    public MessageType MessageType;
    public int Number;
    public int DeviceID;
    public UsbmuxDevice Properties;

    public enum MessageType {
        Attached, Detached, Result, Paired
    }
}
