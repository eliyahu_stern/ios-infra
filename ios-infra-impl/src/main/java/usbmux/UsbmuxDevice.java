package usbmux;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsbmuxDevice {
    public final int ConnectionSpeed;
    public final String ConnectionType;
    public final int DeviceID;
    public final long LocationID;
    public final int ProductID;
    public final String SerialNumber;

    public UsbmuxDevice(
            @JsonProperty("ConnectionSpeed") int connectionSpeed,
            @JsonProperty("ConnectionType") String connectionType,
            @JsonProperty("DeviceID") int deviceID,
            @JsonProperty("LocationID") long locationID,
            @JsonProperty("ProductID") int productID,
            @JsonProperty("SerialNumber") String serialNumber) {
        ConnectionSpeed = connectionSpeed;
        ConnectionType = connectionType;
        DeviceID = deviceID;
        LocationID = locationID;
        ProductID = productID;
        SerialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return String.format("[%s DeviceID: %d, SerialNumber: %s]",
                getClass().getSimpleName(), DeviceID, SerialNumber);
    }
}
