package usbmux;

import com.dd.plist.*;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static util.PlistUtils.NSObjectToJavaObject;

public class UsbmuxProtocolMessage {
    private static final Logger log = LoggerFactory.getLogger(UsbmuxProtocolMessage.class);
    private static final int HEADER_SIZE = 16;
    private final int version;
    private final int type;
    private final int tag;
    private final byte[] payload;

    private AtomicReference<NSObject> plistMessage = new AtomicReference<>(null);

    private UsbmuxProtocolMessage(int version, int type, int tag, byte[] payload) {
        this.version = version;
        this.type = type;
        this.tag = tag;
        this.payload = payload;
    }

    public UsbmuxProtocolMessage(NSObject plistMessage) {
        this(1, MessageType.MESSAGE_PLIST.code, 0, plistMessage.toXMLPropertyList().getBytes());
        this.plistMessage.set(plistMessage);
    }

    public UsbmuxProtocolMessage(ByteBuf buf) throws IllegalStateException {
        if (buf.readableBytes() < HEADER_SIZE) {
            throw new IllegalStateException(String.format("ByteBuf not ready for processing (%d < %d)", buf.readableBytes(), HEADER_SIZE));
        }

        int overallSize = buf.readIntLE();
        this.version = buf.readIntLE();
        this.type = buf.readIntLE();
        this.tag = buf.readIntLE();
        this.payload = new byte[overallSize - HEADER_SIZE];
        if (buf.readableBytes() < overallSize - HEADER_SIZE) {
            throw new IllegalStateException(String.format("ByteBuf not ready for processing (%d < %d)", buf.readableBytes(), overallSize - HEADER_SIZE));
        }
        buf.readBytes(this.payload);
    }

    public int getVersion() {
        return version;
    }

    public int getType() {
        return type;
    }

    public int getTag() {
        return tag;
    }

    public byte[] getPayload() {
        return payload;
    }

    int getSize() {
        return payload.length;
    }

    public synchronized NSObject getPlistMessage() throws IOException, PropertyListFormatException, ParserConfigurationException, ParseException, SAXException, UsbmuxMessageTypeMismatchException {
        if (type != MessageType.MESSAGE_PLIST.code) throw new UsbmuxMessageTypeMismatchException("not a plist");
        if (plistMessage.get() == null) {
            plistMessage.compareAndSet(null, PropertyListParser.parse(payload));
        }

        return plistMessage.get();
    }

    public void writeToByteBuffer(ByteBuf out, int newTag) {
        out.writeIntLE(getSize() + HEADER_SIZE);
        out.writeIntLE(getVersion());
        out.writeIntLE(getType());
        out.writeIntLE(newTag);

        out.writeBytes(getPayload());
    }

    @Override
    public String toString() {
        if (type == MessageType.MESSAGE_PLIST.code) {
            try {

                return String.format("Usbmuxd plist message:\n%s\n", NSObjectToJavaObject(getPlistMessage()));
            } catch (UsbmuxMessageTypeMismatchException e) {
                log.warn("Trying to stringify plist from non-plist message", e);
            } catch (IOException | ParserConfigurationException | SAXException | ParseException | PropertyListFormatException e) {
                log.warn("could not parse plist", e);
            }
        }

        return String.format("[%s version: %d, type: %s, tag: %d, payload length: %d]",
                this.getClass().getSimpleName(),
                version,
                MessageType.byCode(type).name(),
                tag,
                payload.length);
    }

    public enum MessageType {
        /**
         * DO NOT CHANGE THE ORDER OF THIS ENUM!
         * IT USES THE ORDINAL VALUE AS KEY.
         */
        MESSAGE_RESULT,         // 1
        MESSAGE_CONNECT,        // 2
        MESSAGE_LISTEN,         // 3
        MESSAGE_DEVICE_ADD,     // 4
        MESSAGE_DEVICE_REMOVE,  // 5
        MESSAGE_DEVICE_PAIRED,  // 6
        MESSAGE_UNKNOWN,        // Probably just reserved, used here to fix ordinal()
        MESSAGE_PLIST;          // 8

        public final int code;

        MessageType() {
            this.code = this.ordinal() + 1;
        }

        /**
         * Get the message type constant for the given code.
         * @param code the integer code of the required constant.
         * @return the enum const, or MESSAGE_UNKNOWN.
         */
        public static MessageType byCode(int code) {
            return code > 0 && code <= values().length ? values()[code - 1] : MESSAGE_UNKNOWN;
        }
    }

    private class UsbmuxMessageTypeMismatchException extends RuntimeException {
        public UsbmuxMessageTypeMismatchException(String msg) {
            super(msg);
        }
    }
}
