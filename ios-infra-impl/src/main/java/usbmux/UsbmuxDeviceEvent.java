package usbmux;

public class UsbmuxDeviceEvent {
    public int DeviceID;
    public UsbmuxDeviceSerializedMessage.MessageType MessageType;
    public UsbmuxDevice Properties;
}
