package usbmux;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class UsbmuxProtocolCodec extends ByteToMessageCodec<UsbmuxProtocolMessage> {
    private AtomicInteger tag = new AtomicInteger(0);

    @Override
    protected void encode(ChannelHandlerContext ctx, UsbmuxProtocolMessage msg, ByteBuf out) throws Exception {
        msg.writeToByteBuffer(out, tag.getAndIncrement());

        flush(ctx);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        while (in.readableBytes() > 0) {
            in.markReaderIndex();
            try {
                out.add(new UsbmuxProtocolMessage(in));
            } catch (IllegalStateException lse) {
                in.resetReaderIndex();
                return;
            }
        }
    }
}
