package usbmux;

import com.dd.plist.NSDictionary;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.kqueue.KQueueDomainSocketChannel;
import io.netty.channel.kqueue.KQueueEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.unix.DomainSocketAddress;
import io.netty.util.concurrent.Promise;
import service.DeviceServiceClient;
import service.DeviceServiceClientFactory;
import service.KnownDeviceServiceClient;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.BufferOverflowException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static util.PlistUtils.NSObjectToInstanceOf;

public class UsbmuxNettyClient {
    private final Channel channel;
    private final String name;

    private BlockingQueue<Promise<UsbmuxDeviceSerializedMessage>> requestList = new ArrayBlockingQueue<>(16);
    private BiConsumer<UsbmuxDevice, UsbmuxDeviceSerializedMessage.MessageType> deviceListener;
    private final List<UsbmuxDevice> devices = new LinkedList<>();

    private final EventLoopGroup workerGroup;

    public UsbmuxNettyClient(String name) throws IOException, InterruptedException {
        this.name = name;

        Bootstrap bootstrap;
        SocketAddress remoteAddress;
        if (System.getProperty("os.name").contains("OS X")) {
            remoteAddress = new DomainSocketAddress(new File("/var/run/usbmuxd"));

            workerGroup = new KQueueEventLoopGroup();

            bootstrap = new Bootstrap()
                    .group(workerGroup)
                    .channel(KQueueDomainSocketChannel.class);
        } else if (System.getProperty("os.name").contains("Windows")) {
            remoteAddress = new InetSocketAddress(InetAddress.getLocalHost(), 27015);

            workerGroup = new DefaultEventLoopGroup();

            bootstrap = new Bootstrap()
                    .group(workerGroup)
                    .channel(NioSocketChannel.class);
        } else {
            throw new UnsupportedOperationException("USBMuxd connection is not supported for this OS: " +
                    System.getProperty("os.name"));
        }

        bootstrap.handler(new ChannelInitializer<Channel>() {
            @Override
            protected void initChannel(Channel ch) {

            }
        });

        channel = bootstrap.connect(remoteAddress).sync().channel();
//        channel.pipeline().addLast(new LoggingHandler(LogLevel.TRACE));
        channel.pipeline().addLast(new UsbmuxProtocolCodec());
        channel.pipeline().addLast(new SimpleChannelInboundHandler<UsbmuxProtocolMessage>() {
            @Override
            protected void channelRead0(ChannelHandlerContext ctx, UsbmuxProtocolMessage msg) throws Exception {
                synchronized(this){
                    UsbmuxDeviceSerializedMessage serializedMessage =
                            NSObjectToInstanceOf(msg.getPlistMessage(), UsbmuxDeviceSerializedMessage.class);
                    Promise<UsbmuxDeviceSerializedMessage> promise;
                    if(requestList != null && (promise = requestList.poll()) != null) {
                        promise.setSuccess(serializedMessage);
                    } else if (serializedMessage.MessageType != null && deviceListener != null) {
                        Optional<UsbmuxDevice> dev = Optional.ofNullable(serializedMessage.Properties);
                        switch (serializedMessage.MessageType) {
                            case Attached:
                                dev.ifPresent(devices::add);
                                break;
                            case Detached:
                                dev = devices.stream().filter(device -> device.DeviceID == serializedMessage.DeviceID).findFirst();
                                dev.ifPresent(devices::remove);
                                break;
                            case Paired:
                                dev = devices.stream().filter(device -> device.DeviceID == serializedMessage.DeviceID).findFirst();
                                break;
                            case Result:
                                break;
                        }

                        dev.ifPresent(d -> deviceListener.accept(d, serializedMessage.MessageType));
                    } else {
                        // ???
                    }
                }
            }

            @Override
            public void channelInactive(ChannelHandlerContext ctx) throws Exception {
                super.channelInactive(ctx);
                synchronized(this){
                    Promise<UsbmuxDeviceSerializedMessage> prom;
                    Exception err = null;
                    while((prom = requestList.poll()) != null)
                        prom.setFailure(err != null ? err :
                                (err = new IOException("Connection lost")));
                    requestList = null;
                }
            }
        });
    }

    private NSDictionary createPlistMessage(String messageType) {
        NSDictionary plist = new NSDictionary();
        plist.put("BundleID", "com.experitest.ios-infra");
        plist.put("ClientVersionString", "Experitest's USB multiplexer client");
        plist.put("MessageType", messageType);
        plist.put("ProgName", "Experitest");
        plist.put("kLibUSBMuxVersion", 3);

        return plist;
    }

    public void listen(BiConsumer<UsbmuxDevice, UsbmuxDeviceSerializedMessage.MessageType> deviceListener) throws ExecutionException, InterruptedException {
        this.deviceListener = deviceListener;
        NSDictionary dict = createPlistMessage("Listen");
        sendMessage(new UsbmuxProtocolMessage(dict)).get();
    }

    public List<UsbmuxDevice> list() throws ExecutionException, InterruptedException {
        UsbmuxDeviceSerializedMessage msg = listInner().get();
        if (msg.DeviceList != null)
            return msg.DeviceList.stream().map(m -> m.Properties).collect(Collectors.toList());
//        switch(msg.MessageType) {
//            case Attached:
//                break;
//            case Detached:
//                break;
//            case Result:
//                break;
//        }

        return null;
    }

    private Future<UsbmuxDeviceSerializedMessage> listInner() {
        NSDictionary dict = createPlistMessage("ListDevices");
        return sendMessage(new UsbmuxProtocolMessage(dict));
    }

    public Object readBUID() {
        return null;
    }

    public void savePairRecord(String recordID, String recordData) {

    }

    public String readPairRecord(String recordID) {
        return null;
    }

    public void deletePairRecord(String recordID) {

    }

    public DeviceServiceClient connect(
            UsbmuxDevice device,
            KnownDeviceServiceClient knownService) {
        // Connect by known name, API and protocol

        int port = -1; // get the port

        return knownService.createClient(device, port, channel, name);
    }

    public <T extends DeviceServiceClient> T connect(
            UsbmuxDevice device,
            int port,
            DeviceServiceClientFactory<T> clientFactory) {
        return clientFactory.createClient(device, port, channel, name);
    }

    public Future<UsbmuxDeviceSerializedMessage> sendMessage(UsbmuxProtocolMessage message) {
        return sendMessage(message, workerGroup.next().newPromise());
    }

    public Future<UsbmuxDeviceSerializedMessage> sendMessage(UsbmuxProtocolMessage message, Promise<UsbmuxDeviceSerializedMessage> prom) {
        synchronized(this){
            if(requestList == null) {
                // Connection closed
                prom.setFailure(new IllegalStateException());
            } else if(requestList.offer(prom)) {
                // Connection open and message accepted
                channel.writeAndFlush(message);
            } else {
                // Connection open and message rejected
                prom.setFailure(new BufferOverflowException());
            }
            return prom;
        }
    }
}
