package lockdown;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

public class LockdownProtocolCodec extends ByteToMessageCodec<LockdownProtocolMessage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, LockdownProtocolMessage msg, ByteBuf out) throws Exception {

    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

    }
}
