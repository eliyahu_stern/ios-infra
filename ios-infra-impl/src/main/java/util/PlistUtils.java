package util;

import com.dd.plist.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

import static com.dd.plist.NSNumber.*;

public class PlistUtils {
    /* START SECTION: convert NSObjects to Java collections / primitives */
    // This is done since the original implementation in dd-plist uses arrays,
    // which don't allow convenient logging.

    private static List<Object> deserializeArray(NSArray array) {
        NSObject[] originalArray = array.getArray();
        List<Object> clonedArray = new ArrayList<>(originalArray.length);

        for (NSObject anOriginalArray : originalArray) {
            clonedArray.add(NSObjectToJavaObject(anOriginalArray));
        }

        return clonedArray;
    }

    private static Map<String, Object> deserializeMap(NSDictionary map) {
        Map<String, NSObject> originalMap = map.getHashMap();
        Map<String, Object> clonedMap = new HashMap<>(originalMap.size());

        for (String key : originalMap.keySet()) {
            clonedMap.put(key, NSObjectToJavaObject(originalMap.get(key)));
        }

        return clonedMap;
    }

    private static Set<Object> deserializeSet(NSSet set) {
        Set<Object> clonedSet = new HashSet<>();
        set.objectIterator().forEachRemaining((nsObject -> clonedSet.add(NSObjectToJavaObject(nsObject))));

        return clonedSet;
    }

    private static Object deserializeNumber(NSNumber num) {
        switch(num.type()) {
            case INTEGER:
                long longVal = num.longValue();
                if (longVal <= Integer.MAX_VALUE && longVal >= Integer.MIN_VALUE) {
                    return num.intValue();
                }

                return longVal;
            case REAL:
                return num.doubleValue();
            case BOOLEAN:
                return num.boolValue();
            default:
                return num.doubleValue();
        }
    }

    public static Object NSObjectToJavaObject(NSObject obj) {
        if (obj instanceof NSArray) {
            return deserializeArray((NSArray) obj);
        } else if (obj instanceof NSDictionary) {
            return deserializeMap((NSDictionary) obj);
        } else if (obj instanceof NSSet) {
            return deserializeSet((NSSet) obj);
        } else if (obj instanceof NSNumber) {
            return deserializeNumber((NSNumber) obj);
        } else if (obj instanceof NSString) {
            return ((NSString)obj).getContent();
        } else if (obj instanceof NSData) {
            return ((NSData)obj).bytes();
        } else if (obj instanceof NSDate) {
            return ((NSDate)obj).getDate();
        } else {

            return obj instanceof UID ? obj.toXMLPropertyList() : obj;
        }
    }

    /* END SECTION: convert NSObjects to Java collections / primitives */

    public static <T> T NSObjectToInstanceOf(NSObject obj, Class<T> clazz) throws ReflectiveOperationException {

        Object jobj = obj.toJavaObject();

        final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
        return mapper.convertValue(jobj, clazz);
    }
}
