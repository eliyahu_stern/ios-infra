package service;

import io.netty.channel.Channel;
import usbmux.UsbmuxDevice;

public interface DeviceServiceClientFactory<T extends DeviceServiceClient> {
    T createClient(UsbmuxDevice device, int port, Channel channel, String name);
}
